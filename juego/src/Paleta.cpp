#include "Paleta.h"
#include <OgreSceneNode.h>

Paleta::Paleta(SceneNode* node) {
	_node = node;
	_velocidad_paleta = VEL_PALETA;
}

Paleta::Paleta(const Paleta &obj){
	_node = obj.getNode();
}

Paleta::~Paleta() {
	delete _node;
}

Paleta& Paleta::operator= (const Paleta &obj){
	delete _node;
	_node = obj._node;
	return *this;
}

SceneNode* Paleta::getNode()const{
	return _node;
}

void Paleta::movRight(){
	if(_node->getPosition().z - (TAM_PALETAH/2) > -LIMITE_VERTICAL + TAM_LIMITE_VERTICAL/2){ //se comprueba el limite derecho
		_node->translate(0,0,-_velocidad_paleta);
	}
}

void Paleta::movLeft(){
	if(_node->getPosition().z + (TAM_PALETAH/2) < LIMITE_VERTICAL - TAM_LIMITE_VERTICAL/2){ //se comprueba el limite izquierdo
		_node->translate(0,0,_velocidad_paleta);
	}
}
