#include "DificultadState.h"

#include "../include/RecordsState.h"
#include "PlayState.h"
#include "../include/CreditosState.h"
#include "IntroState.h"

template<> DificultadState* Ogre::Singleton<DificultadState>::msSingleton = 0;

void
DificultadState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(0.01, 20, 0));
  _camera->lookAt(Ogre::Vector3(0, 0, 0));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(100);
  _camera->setFOVy(Ogre::Degree(50));
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

  crearMenu();

  _menuSeleccion = 0;
  cambiarTexturaMenu();

  _exitGame = false;
}

void
DificultadState::exit()
{
  if (_root->hasSceneManager("SceneManager")){
	  _sceneMgr->clearScene();
  }
  _root->getAutoCreatedWindow()->removeAllViewports();
}

void
DificultadState::pause ()
{
}

void
DificultadState::resume ()
{
}

bool
DificultadState::frameStarted
(const Ogre::FrameEvent& evt)
{
  return true;
}

bool
DificultadState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;

  return true;
}

void
DificultadState::keyPressed
(const OIS::KeyEvent &e)
{
  //transicion al PlayState
  if (e.key == 28 && _menuSeleccion == 0) {
	  _dificultad = 1;
    changeState(PlayState::getSingletonPtr());
  }

  if (e.key == 28 && _menuSeleccion == 1) {
	  _dificultad = 2;
	  changeState(PlayState::getSingletonPtr());
  }

  if (e.key == 28 && _menuSeleccion == 2) {
	  _dificultad = 3;
  	  changeState(PlayState::getSingletonPtr());
    }

  if (e.key == OIS::KC_UP && _menuSeleccion > 0){
  	  _menuSeleccion--;
  	  cambiarTexturaMenu();
    }

  if (e.key == OIS::KC_DOWN && _menuSeleccion < 2){
	  _menuSeleccion++;
	  cambiarTexturaMenu();
  }
}

void
DificultadState::keyReleased
(const OIS::KeyEvent &e )
{
  if (e.key == OIS::KC_ESCAPE) {
	  _root->getAutoCreatedWindow()->removeAllViewports();
	  _sceneMgr->destroyAllCameras();
	  _sceneMgr->clearScene();
	  _root->destroySceneManager(_sceneMgr);
	  changeState(IntroState::getSingletonPtr());
  }
}

void
DificultadState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
DificultadState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
DificultadState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void DificultadState::crearMenu(){
	Ogre::SceneNode *node_menu = _sceneMgr->createSceneNode("Menu");
	_sceneMgr->getRootSceneNode()->addChild(node_menu);

	//Nodo PlanoMenu
	Ogre::SceneNode *node_PlanoMenu = _sceneMgr->createSceneNode("PlanoMenu");
	Ogre::Entity* ent_PlanoMenu = _sceneMgr->createEntity("PlanoMenu", "PlanoMenu.mesh");
	node_PlanoMenu->attachObject(ent_PlanoMenu);
	node_menu->addChild(node_PlanoMenu);
	node_PlanoMenu->translate(0, -10, 0);

	//Nodo Facil
	Ogre::SceneNode *node_facil = _sceneMgr->createSceneNode("Facil");
	Ogre::Entity* ent_facil = _sceneMgr->createEntity("Facil", "Plane.mesh");
	node_facil->attachObject(ent_facil);
	node_menu->addChild(node_facil);
	node_facil->translate(-5.0, 0, 0);
	node_facil->yaw(Ogre::Degree(90));
	ent_facil->setMaterialName("MaterialMenuA5");

	//Nodo Normal
	Ogre::SceneNode *node_normal = _sceneMgr->createSceneNode("Normal");
	Ogre::Entity* ent_normal = _sceneMgr->createEntity("Normal", "Plane.mesh");
	node_normal->attachObject(ent_normal);
	node_menu->addChild(node_normal);
	node_normal->yaw(Ogre::Degree(90));
	ent_normal->setMaterialName("MaterialMenuA6");

	//Nodo Dificil
	Ogre::SceneNode *node_dificil = _sceneMgr->createSceneNode("Dificil");
	Ogre::Entity* ent_dificil = _sceneMgr->createEntity("Dificil", "Plane.mesh");
	node_dificil->attachObject(ent_dificil);
	node_menu->addChild(node_dificil);
	node_dificil->translate(5.0, 0, 0);
	node_dificil->yaw(Ogre::Degree(90));
	ent_dificil->setMaterialName("MaterialMenuA7");
}



void DificultadState::cambiarTexturaMenu(){
	switch (_menuSeleccion){
	case 0:
		_sceneMgr->getEntity("Facil")->setMaterialName("MaterialMenuR5");
		_sceneMgr->getEntity("Normal")->setMaterialName("MaterialMenuA6");
		break;
	case 1:
		_sceneMgr->getEntity("Normal")->setMaterialName("MaterialMenuR6");
		_sceneMgr->getEntity("Facil")->setMaterialName("MaterialMenuA5");
		_sceneMgr->getEntity("Dificil")->setMaterialName("MaterialMenuA7");
		break;
	case 2:
		_sceneMgr->getEntity("Dificil")->setMaterialName("MaterialMenuR7");
		_sceneMgr->getEntity("Normal")->setMaterialName("MaterialMenuA6");
		break;
	}
}

int DificultadState::getDificultad(){
	return _dificultad;
}

DificultadState*
DificultadState::getSingletonPtr ()
{
return msSingleton;
}

DificultadState&
DificultadState::getSingleton ()
{
  assert(msSingleton);
  return *msSingleton;
}
