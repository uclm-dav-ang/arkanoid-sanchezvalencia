#include "Bola.h"
#include <OgreSceneNode.h>
#include <cmath>
#include "DificultadState.h"

Bola::Bola(SceneNode* node, std::list<Bloque*> vector_bloques, std::list<Salvavidas*> lista_salvavidas, Paleta *paleta) {
	_node = node;
	_vector_bloques = vector_bloques;
	_lista_salvavidas = lista_salvavidas;
	_Z = 0.07;
	switch(DificultadState::getSingletonPtr()->getDificultad()){
	case 1:
		_modulo = MODULO_F;
		_limite_velocidad_z = LIM_VEL_ZF;
		break;
	case 2:
		_modulo = MODULO_N;
		_limite_velocidad_z = LIM_VEL_ZN;
		break;
	case 3:
		_modulo = MODULO_D;
		_limite_velocidad_z = LIM_VEL_ZD;
		break;
	}

	calcularX();
	_paleta = paleta;
	_vidas = 3;
	_puntos = 0;
	_gameManager = GameManager::getSingletonPtr();
}

Bola::Bola(const Bola &obj){
	_node = obj.getNode();
}

Bola::~Bola() {
	delete _node;
}

Bola& Bola::operator= (const Bola &obj){
	delete _node;
	_node = obj._node;
	return *this;
}

SceneNode* Bola::getNode()const{
	return _node;
}

bool Bola::compruebaColision(){
	//colision con los bloques
	std::list<Bloque*>::iterator it;
	for(it = _vector_bloques.begin(); it != _vector_bloques.end(); it++){
		Bloque *aux = *it;
		if ((((aux->getNode()->getPosition().x + TAM_BLOCK_V/2 > _node->getPosition().x-TAM_BOLA/2) && (_node->getPosition().x > aux->getNode()->getPosition().x + TAM_BLOCK_V/2)) || ((_node->getPosition().x + TAM_BOLA/2 > aux->getNode()->getPosition().x - TAM_BLOCK_V/2) && (_node->getPosition().x < aux->getNode()->getPosition().x - TAM_BLOCK_V/2))) && ((_node->getPosition().z - TAM_BOLA/2 < aux->getNode()->getPosition().z + TAM_BLOCK_H/2 && _node->getPosition().z - TAM_BOLA/2 > aux->getNode()->getPosition().z - TAM_BLOCK_H/2) || (_node->getPosition().z + TAM_BOLA/2 < aux->getNode()->getPosition().z + TAM_BLOCK_H/2 && _node->getPosition().z + TAM_BOLA/2 > aux->getNode()->getPosition().z - TAM_BLOCK_H/2))){
			cambiarSentidoX();
			aux->decrementnGolpes();
			incrementarPuntos();
			if(aux->getnGolpes() == 0){
				_gameManager->getSoundFXPtr()->play(); //reproducimos el sonido
				aux->getNode()->detachAllObjects();
				_vector_bloques.remove(aux);
			}
			break;
		} else if ((((aux->getNode()->getPosition().z + TAM_BLOCK_H/2 > _node->getPosition().z-TAM_BOLA/2) && (_node->getPosition().z > aux->getNode()->getPosition().z + TAM_BLOCK_H/2)) || ((_node->getPosition().z + TAM_BOLA/2 > aux->getNode()->getPosition().z - TAM_BLOCK_H/2) && (_node->getPosition().z < aux->getNode()->getPosition().z - TAM_BLOCK_H/2))) && ((_node->getPosition().x - TAM_BOLA/2 < aux->getNode()->getPosition().x + TAM_BLOCK_V/2 && _node->getPosition().x - TAM_BOLA/2 > aux->getNode()->getPosition().x - TAM_BLOCK_V/2) || (_node->getPosition().x + TAM_BOLA/2 < aux->getNode()->getPosition().x + TAM_BLOCK_V/2 && _node->getPosition().x + TAM_BOLA/2 > aux->getNode()->getPosition().x - TAM_BLOCK_V/2))) {
			cambiarSentidoZ();
			aux->decrementnGolpes();
			incrementarPuntos();
			if(aux->getnGolpes() == 0){
				_gameManager->getSoundFXPtr()->play(); //reproducimos el sonido
				aux->getNode()->detachAllObjects();
				_vector_bloques.remove(aux);
			}
			break;
		}
	}

	//colision con la paleta
	if((_paleta->getNode()->getPosition().x - TAM_PALETAV/2 < _node->getPosition().x + TAM_BOLA/2) && (_node->getPosition().x < _paleta->getNode()->getPosition().x) && (_paleta->getNode()->getPosition().z + TAM_PALETAH/2 > _node->getPosition().z - TAM_BOLA/2) && (_paleta->getNode()->getPosition().z - TAM_PALETAH/2 < _node->getPosition().z + TAM_BOLA/2) && (_X > 0)){
		calcularZ();
		calcularX();
		cambiarSentidoX();
	}

	//colision con el limite vertical de la pantalla
	if(_node->getPosition().z + TAM_BOLA/2 > LIMITE_VERTICAL - TAM_LIMITE_VERTICAL/2 || _node->getPosition().z - TAM_BOLA/2 < -LIMITE_VERTICAL + TAM_LIMITE_VERTICAL/2){
		cambiarSentidoZ();
	}

	//colision con el limite superior
	if(_node->getPosition().x - TAM_BOLA/2 < -LIMITE_SUPERIOR + TAM_LIMITE_VERTICAL/2){
		cambiarSentidoX();
	}

	//colision con los salvavidas
	std::list<Salvavidas*>::iterator it2;
	for(it2 = _lista_salvavidas.begin(); it2 != _lista_salvavidas.end(); it2++){
		Salvavidas *aux2 = *it2;
		if((aux2->getNode()->getPosition().x - TAM_SALVAVIDAS_V/2 < _node->getPosition().x+TAM_BOLA/2) && (_node->getPosition().x < aux2->getNode()->getPosition().x) && ((_node->getPosition().z - TAM_BOLA/2 < aux2->getNode()->getPosition().z + TAM_SALVAVIDAS_H/2 && _node->getPosition().z - TAM_BOLA/2 > aux2->getNode()->getPosition().z - TAM_SALVAVIDAS_H/2) || (_node->getPosition().z + TAM_BOLA/2 < aux2->getNode()->getPosition().z + TAM_SALVAVIDAS_H/2 && _node->getPosition().z + TAM_BOLA/2 > aux2->getNode()->getPosition().z - TAM_SALVAVIDAS_H/2))){
			cambiarSentidoX();
			aux2->getNode()->detachAllObjects();
			_lista_salvavidas.remove(aux2);
			break;
		}
	}

	//colision con el final de la pantalla
	if(_node->getPosition().x > 8.4){
		decrementarVidas();
		_node->setPosition(4.25,0,0);
		cambiarSentidoX();
		return false;
	}
	return true;
}

void Bola::mover(){
	_node->translate(_X,0,_Z);
}

void Bola::cambiarSentidoX(){
	_X = _X * (-1);
}

void Bola::cambiarSentidoZ(){
	_Z = _Z * (-1);
}

//calculamos la x dependiendo de la z para que mantenga el mismo módulo
void Bola::calcularX(){
	_X =sqrt(abs(pow(_modulo,2) - pow(_Z,2)));
}

//calculamos la z dependiendo de la posicion de la paleta donde choque la bola
void Bola::calcularZ(){
	double posBola = _node->getPosition().z;
	double posPaleta = _paleta->getNode()->getPosition().z;
	double factor = (posBola - posPaleta);
	_Z = _Z + (factor/1.25 * 0.1)/1.25;

	if (_Z > _limite_velocidad_z){
		_Z = _limite_velocidad_z;
	} else if (_Z < -_limite_velocidad_z){
		_Z = -_limite_velocidad_z;
	}
}

void Bola::decrementarVidas(){
	_vidas--;
}

void Bola::incrementarPuntos(){
	_puntos += 10;
}

bool Bola::isLose(){
	if(_vidas == 0){
		return true;
	}

	return false;
}

bool Bola::isWin(){
	if (_vector_bloques.empty()){
		return true;
	}

	return false;
}

int Bola::getPuntos(){
	return _puntos;
}

int Bola::getVidas(){
	return _vidas;
}

void Bola::setBloques(std::list<Bloque*> vector_bloques){
	_vector_bloques = vector_bloques;
}

void Bola::setNode(SceneNode* node){
	_node = node;
}

void Bola::setPaleta(Paleta *paleta){
	_paleta = paleta;
}
