#include "PlayState.h"
#include "PauseState.h"
#include "WinState.h"
#include "LoseState.h"
#include "IntroState.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void PlayState::enter () {
	//limpiamos la lista de los bloques
	_vector_bloques.clear();

	//volvemos a hacer visibles los elementos salvavidas
	std::list<Salvavidas*>::iterator it;
	for(it = _lista_salvavidas.begin(); it != _lista_salvavidas.end(); it++){
		Salvavidas *aux = *it;
		aux->getNode()->setVisible(true);
	}


	_nivel++; //actualizamos el nivel en que nos encontramos
	_root = Ogre::Root::getSingletonPtr();


	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE);

	_movimientoBola = false;
	_movPaletaRight = false;
	_movPaletaLeft = false;

	_camera = _sceneMgr->getCamera("IntroCamera");
	_camera->setPosition(Ogre::Vector3(11.2, 9, 0));
	_camera->lookAt(Ogre::Vector3(3, 0, 0));
	_camera->setNearClipDistance(5);
	_camera->setFarClipDistance(100);
	_camera->setFOVy(Ogre::Degree(50));
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);


	//creamos un punto de luz
	Light* light = _sceneMgr->createLight("Light");
	light->setPosition(3, 12, 0);
	light->setDiffuseColour(0.2, 0.2, 0.2);
	light->setType(Light::LT_SPOTLIGHT);
	light->setDirection(Vector3(-0.3, -1, 0));
	light->setSpotlightInnerAngle(Degree(25.0f));
	light->setSpotlightOuterAngle(Degree(60.0f));
	light->setSpotlightFalloff(5.0f);
	light->setCastShadows(true);

	//Creamos el nodo principal del que colgarán los demás
	Ogre::SceneNode* node_principal = _sceneMgr->createSceneNode("principal");
	_sceneMgr->getRootSceneNode()->addChild(node_principal);

	//Creamos el nodo para la paleta
	_node_paleta = _sceneMgr->createSceneNode("Paleta");
	Ogre::Entity* ent = _sceneMgr->createEntity("Paleta", "Paleta.mesh");
	_node_paleta->attachObject(ent);
	node_principal->addChild(_node_paleta);
	_node_paleta->translate(5, 0, 0);
	_paleta = new Paleta(_node_paleta);

	//Creamos el nodo bola
	_node_bola = _sceneMgr->createSceneNode("Bola");
	Ogre::Entity* ent_bola = _sceneMgr->createEntity("Bola", "Bola.mesh");
	_node_bola->attachObject(ent_bola);
	node_principal->addChild(_node_bola);
	_node_bola->translate(4.25, 0, 0);

	//Creamos limite superior
	_node_limite_superior = _sceneMgr->createSceneNode("Limite");
	Ogre::Entity* ent_limite = _sceneMgr->createEntity("Limite", "Limite.mesh");
	_node_limite_superior->attachObject(ent_limite);
	node_principal->addChild(_node_limite_superior);
	_node_limite_superior->translate(-LIMITE_SUPERIOR, 0, 0);
	_node_limite_superior->scale(1, 1, 1.6);

	//Creamos limite derecho
	_node_limite_derecho = _sceneMgr->createSceneNode("LimiteDerecho");
	Ogre::Entity* ent_limite_derecho = _sceneMgr->createEntity("LimiteDerecho",	"Limite.mesh");
	_node_limite_derecho->attachObject(ent_limite_derecho);
	node_principal->addChild(_node_limite_derecho);
	_node_limite_derecho->yaw(Ogre::Degree(90));
	_node_limite_derecho->translate(-1, 0, -LIMITE_VERTICAL);
	_node_limite_derecho->scale(1, 1, 1.6);

	//Creamos limite izquierdo
	_node_limite_izquierdo = _sceneMgr->createSceneNode("LimiteIzquierdo");
	Ogre::Entity* ent_limite_izquierdo = _sceneMgr->createEntity("LimiteIzquierdo", "Limite.mesh");
	_node_limite_izquierdo->attachObject(ent_limite_izquierdo);
	node_principal->addChild(_node_limite_izquierdo);
	_node_limite_izquierdo->yaw(Ogre::Degree(90));
	_node_limite_izquierdo->translate(-1, 0, LIMITE_VERTICAL);
	_node_limite_izquierdo->scale(1, 1, 1.6);

	//Creamos el plano space
	_node_space = _sceneMgr->createSceneNode("BackgroundSpace");
	Ogre::Entity* ent_space = _sceneMgr->createEntity("BackgroundSpace", "BackgroundSpace.mesh");
	_node_space->attachObject(ent_space);
	node_principal->addChild(_node_space);
	_node_space->translate(0, -5, 0);

	//Creamos el plano game
	_node_game = _sceneMgr->createSceneNode("BackgroundGame");
	Ogre::Entity* ent_game = _sceneMgr->createEntity("BackgroundGame", "BackgroundGame.mesh");
	ent_game->setCastShadows(false);
	_node_game->attachObject(ent_game);
	node_principal->addChild(_node_game);
	_node_game->translate(-2.5, -2, 0);


	std::stringstream saux;
	std::string s = "Ladrillo";
	double x, z;
	int i = 1;

	if(_nivel == 1){
		_lista_salvavidas.clear();
		for (x = -3.4; x > -7.4; x -= 1.2) {
			for (z = 5.4; z >= -5.4; z -= 1.2) {
				saux << s << i;
				i++;
				Ogre::SceneNode* node_block = _sceneMgr->createSceneNode(saux.str());
				Ogre::Entity* ent2 = _sceneMgr->createEntity(saux.str(), "Ladrillo.mesh");
				node_block->setPosition(x, 0, z);

				//Insertamos los bloques en una lista
				Bloque *bloqueaux = new Bloque(node_block,1);
				_vector_bloques.push_back(bloqueaux);

				node_block->attachObject(ent2);
				node_principal->addChild(node_block);

				saux.str("");
			}
		}

		//creamos los salvavidas
			std::stringstream saux2;
			std::string s2 = "Salvavidas";
			double x2 = 7.3;
			int j = 1;
			double z2;

			for (z2 = 5.4; z2 >= -5.4; z2 -= 1.2) {
				saux2 << s2 << j;
				j++;
				Ogre::SceneNode* node_salvavidas = _sceneMgr->createSceneNode(saux2.str());
				Ogre::Entity* ent3 = _sceneMgr->createEntity(saux2.str(), "Salvavidas.mesh");
				node_salvavidas->setPosition(x2, 0, z2);

				//Insertamos los bloques en una lista
				Salvavidas *bloqueaux2 = new Salvavidas(node_salvavidas);
				_lista_salvavidas.push_back(bloqueaux2);

				node_salvavidas->attachObject(ent3);
				_sceneMgr->getRootSceneNode()->addChild(node_salvavidas);

				saux2.str("");
			}

			_bola = new Bola(_node_bola, _vector_bloques, _lista_salvavidas, _paleta);
	}

	if(_nivel == 2){
		for (x = -3.4; x > -7.4; x -= 1.2) {
			for (z = 5.4; z >= -5.4; z -= 2.7) {
				saux << s << i;
				i++;
				Ogre::SceneNode* node_block = _sceneMgr->createSceneNode(saux.str());
				Ogre::Entity* ent2 = _sceneMgr->createEntity(saux.str(),"Ladrillo.mesh");
				node_block->setPosition(x, 0, z);

				//Insertamos los bloques en una lista
				Bloque *bloqueaux = new Bloque(node_block,1);
				_vector_bloques.push_back(bloqueaux);

				node_block->attachObject(ent2);
				node_principal->addChild(node_block);

				saux.str("");
			}
		}

		for (z = 5.4; z >= -5.4; z -= 1.2){
			saux << s << i;
			i++;
			Ogre::SceneNode* node_block = _sceneMgr->createSceneNode(saux.str());
			Ogre::Entity* ent2 = _sceneMgr->createEntity(saux.str(),"Ladrillo.mesh");
			ent2->setMaterialName("Material3");
			node_block->setPosition(0, 0, z);


			//Insertamos los bloques en una lista
			Bloque *bloqueaux = new Bloque(node_block,2);
			_vector_bloques.push_back(bloqueaux);

			node_block->attachObject(ent2);
			node_principal->addChild(node_block);
			ent2->setMaterialName("Material3");
		}
		_bola->setNode(_node_bola);
		_bola->setBloques(_vector_bloques);
		_bola->setPaleta(_paleta);
	}

	if(_nivel == 3){
		for (x = 1.4; x > -1; x -= 1.2) {
			for (z = 5.4; z >= -5.4; z -= 1.2) {
				saux << s << i;
				i++;
				Ogre::SceneNode* node_block = _sceneMgr->createSceneNode(saux.str());
				Ogre::Entity* ent2 = _sceneMgr->createEntity(saux.str(),"Ladrillo.mesh");
				node_block->setPosition(x, 0, z);

				//Insertamos los bloques en una lista
				Bloque *bloqueaux = new Bloque(node_block,1);
				_vector_bloques.push_back(bloqueaux);

				node_block->attachObject(ent2);
				node_principal->addChild(node_block);
			}
		}

		for (x = -2.2; x > -4.4; x -= 1.2) {
			for (z = 5.4; z >= -5.4; z -= 1.2) {
				saux << s << i;
				i++;
				Ogre::SceneNode* node_block = _sceneMgr->createSceneNode(saux.str());
				Ogre::Entity* ent2 = _sceneMgr->createEntity(saux.str(),"Ladrillo.mesh");
				node_block->setPosition(x, 0, z);

				//Insertamos los bloques en una lista
				Bloque *bloqueaux = new Bloque(node_block,2);
				_vector_bloques.push_back(bloqueaux);

				node_block->attachObject(ent2);
				node_principal->addChild(node_block);
				ent2->setMaterialName("Material3");

				saux.str("");
			}
		}

		for (x = -5.6; x > -8.0; x -= 1.2) {
			for (z = 5.4; z >= -5.4; z -= 1.2) {
				saux << s << i;
				i++;
				Ogre::SceneNode* node_block = _sceneMgr->createSceneNode(saux.str());
				Ogre::Entity* ent2 = _sceneMgr->createEntity(saux.str(),"Ladrillo.mesh");
				node_block->setPosition(x, 0, z);

				//Insertamos los bloques en una lista
				Bloque *bloqueaux = new Bloque(node_block,3);
				_vector_bloques.push_back(bloqueaux);

				node_block->attachObject(ent2);
				node_principal->addChild(node_block);
				ent2->setMaterialName("Material5");

				saux.str("");
			}
		}
		_bola->setNode(_node_bola);
		_bola->setBloques(_vector_bloques);
		_bola->setPaleta(_paleta);
	}

	if(_nivel == 4){
		for (x = 0.2; x > -7.4; x -= 2.4) {
			for (z = 5.4; z >= -5.4; z -= 1.2) {
				saux << s << i;
				i++;
				Ogre::SceneNode* node_block = _sceneMgr->createSceneNode(saux.str());
				Ogre::Entity* ent2 = _sceneMgr->createEntity(saux.str(), "Ladrillo.mesh");
				node_block->setPosition(x, 0, z);

				//Insertamos los bloques en una lista
				Bloque *bloqueaux = new Bloque(node_block,10);
				_vector_bloques.push_back(bloqueaux);

				node_block->attachObject(ent2);
				node_principal->addChild(node_block);
				ent2->setMaterialName("Material4");

				saux.str("");
			}
		}

		for (x = -1.0; x > -7.4; x -= 2.4) {
			for (z = 5.4; z >= -5.4; z -= 5.4) {
				saux << s << i;
				i++;
				Ogre::SceneNode* node_block = _sceneMgr->createSceneNode(saux.str());
				Ogre::Entity* ent2 = _sceneMgr->createEntity(saux.str(), "Ladrillo.mesh");
				node_block->setPosition(x, 0, z);

				//Insertamos los bloques en una lista
				Bloque *bloqueaux = new Bloque(node_block,20);
				_vector_bloques.push_back(bloqueaux);

				node_block->attachObject(ent2);
				node_principal->addChild(node_block);
				ent2->setMaterialName("Material6");

				saux.str("");
			}
		}
		_bola->setNode(_node_bola);
		_bola->setBloques(_vector_bloques);
		_bola->setPaleta(_paleta);
	}

	_exitGame = false;

}

void PlayState::exit () {
	if(_sceneMgr->hasEntity("Paleta")) _sceneMgr->destroyEntity("Paleta");
	if(_sceneMgr->hasEntity("Bola")) _sceneMgr->destroyEntity("Bola");
	if(_sceneMgr->hasEntity("Limite")) _sceneMgr->destroyEntity("Limite");
	if(_sceneMgr->hasEntity("LimiteDerecho")) _sceneMgr->destroyEntity("LimiteDerecho");
	if(_sceneMgr->hasEntity("LimiteIzquierdo")) _sceneMgr->destroyEntity("LimiteIzquierdo");
	if(_sceneMgr->hasEntity("BackgroundSpace")) _sceneMgr->destroyEntity("BackgroundSpace");
	if(_sceneMgr->hasEntity("BackgroundGame")) _sceneMgr->destroyEntity("BackgroundGame");
	if(_sceneMgr->hasSceneNode("Paleta")) _sceneMgr->destroySceneNode("Paleta");
	if(_sceneMgr->hasSceneNode("Bola")) _sceneMgr->destroySceneNode("Bola");
	if(_sceneMgr->hasSceneNode("Limite")) _sceneMgr->destroySceneNode("Limite");
	if(_sceneMgr->hasSceneNode("LimiteDerecho")) _sceneMgr->destroySceneNode("LimiteDerecho");
	if(_sceneMgr->hasSceneNode("LimiteIzquierdo")) _sceneMgr->destroySceneNode("LimiteIzquierdo");
	if(_sceneMgr->hasSceneNode("BackgroundSpace")) _sceneMgr->destroySceneNode("BackgroundSpace");
	if(_sceneMgr->hasSceneNode("BackgroundGame")) _sceneMgr->destroySceneNode("BackgroundGame");
	if(_sceneMgr->hasSceneNode("principal")) _sceneMgr->destroySceneNode("principal");
	if(_sceneMgr->hasLight("Light")) _sceneMgr->destroyLight("Light");

	std::list<Bloque*>::iterator it;
	for(it = _vector_bloques.begin(); it != _vector_bloques.end(); it++){
		Bloque *aux = *it;
		if(_sceneMgr->hasEntity(aux->getNode()->getName())) _sceneMgr->destroyEntity(aux->getNode()->getName());
		if(_sceneMgr->hasSceneNode(aux->getNode()->getName())) _sceneMgr->destroySceneNode(aux->getNode());
	}


	std::list<Salvavidas*>::iterator it2;
	for(it2 = _lista_salvavidas.begin(); it2 != _lista_salvavidas.end(); it2++){
		Salvavidas *aux = *it2;
		if(_sceneMgr->hasSceneNode(aux->getNode()->getName())) aux->getNode()->setVisible(false);
	}

  Ogre::OverlayManager::getSingletonPtr()->getByName("Info")->hide();

  _root->getAutoCreatedWindow()->removeAllViewports();
}

void PlayState::pause() {
	_movPaletaRight = false;
	_movPaletaLeft = false;
}

void PlayState::resume() {
  // Se restaura el background colour.
  _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));
}

bool PlayState::frameStarted (const Ogre::FrameEvent& evt) {
	//Movimiento de la pelota
	if(_movimientoBola){
		_bola->mover();
	}

	//Movimiento de la pala
	if(_movPaletaRight){
		_paleta->movRight();
	}

	if(_movPaletaLeft){
		_paleta->movLeft();
	}

	//comprobamos las colisiones
	if(!_bola->compruebaColision()){
		_movimientoBola = false;
	}

	//comprobamos si se ha ganado
	if (_bola->isWin()){
		if (_nivel == 4){
			changeState(LoseState::getSingletonPtr());
		} else {
			changeState(WinState::getSingletonPtr());
		}
	}

	//comprobamos si se ha perdido
	if (_bola->isLose()){
		changeState(LoseState::getSingletonPtr());
	}


	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlay = _overlayManager->getByName("Info");
	overlay->show();

	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("Puntos");
	oe->setCaption(Ogre::StringConverter::toString(_bola->getPuntos()));
	oe = _overlayManager->getOverlayElement("Vidas");
	oe->setCaption(Ogre::StringConverter::toString(_bola->getVidas()));

  return true;
}

bool PlayState::frameEnded (const Ogre::FrameEvent& evt) {
  if (_exitGame)
    return false;
  
  return true;
}

void PlayState::keyPressed (const OIS::KeyEvent &e) {
  // Tecla p --> PauseState.
  if (e.key == OIS::KC_ESCAPE) {
	  pushState(PauseState::getSingletonPtr());
  }

  	 //movemos las palas al pulsar las teclas
  if (e.key == OIS::KC_RIGHT){
	  _movPaletaRight = true;
  }

  if (e.key == OIS::KC_LEFT){
	  _movPaletaLeft = true;
   }

  //al pulsar la tecla b aparece la bola y comienza a moverse
  if (e.key == OIS::KC_SPACE){
	  _node_bola->setVisible(true);
	  _movimientoBola = true;
  }
}

void PlayState::keyReleased (const OIS::KeyEvent &e) {
  //Paramos la pala al soltar las teclas
  if (e.key == OIS::KC_RIGHT){
  	  _movPaletaRight = false;
  }

  if (e.key == OIS::KC_LEFT){
	  _movPaletaLeft = false;
  }

}

void PlayState::mouseMoved (const OIS::MouseEvent &e) {
}

void PlayState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void PlayState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

PlayState* PlayState::getSingletonPtr () {
return msSingleton;
}

PlayState& PlayState::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}

int PlayState::getNivel(){
	return _nivel;
}
Bola* PlayState::getBola(){
	return _bola;
}

std::list<Salvavidas*> PlayState::getSalvavidas(){
	return _lista_salvavidas;
}

void PlayState::setNivel(int nivel){
	_nivel = nivel;
}

void PlayState::setSalvavidas(std::list<Salvavidas*> lista_salvavidas){
	_lista_salvavidas = lista_salvavidas;
}

void PlayState::setMovimientoBola(bool movimientoBola){
	_movimientoBola = movimientoBola;
}

