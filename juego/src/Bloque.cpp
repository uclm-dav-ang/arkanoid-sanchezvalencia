#include "Bloque.h"
#include <OgreSceneNode.h>

Bloque::Bloque(SceneNode* node, int nGolpes) {
	_node = node;
	_nGolpes = nGolpes;
}

Bloque::Bloque(const Bloque &obj){
	_node = obj.getNode();
}

Bloque::~Bloque() {
	delete _node;
}

Bloque& Bloque::operator= (const Bloque &obj){
	delete _node;
	_node = obj._node;
	return *this;
}

SceneNode* Bloque::getNode()const{
	return _node;
}

void Bloque::decrementnGolpes(){
	_nGolpes--;
}

int Bloque::getnGolpes(){
	return _nGolpes;
}
