#include <Ogre.h>
#include <cstdlib>
#include <vector>
using namespace Ogre;
using namespace std;

class Bloque {

private:
	SceneNode *_node;
	int _nGolpes;

public:
  Bloque(SceneNode* node, int nGolpes);
  Bloque(const Bloque &obj);
  ~Bloque();
  Bloque& operator= (const Bloque &obj);
  SceneNode* getNode() const;
  void decrementnGolpes();
  int getnGolpes();
};
