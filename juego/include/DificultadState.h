#ifndef DificultadState_H
#define DificultadState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

class DificultadState : public Ogre::Singleton<DificultadState>, public GameState
{
 public:
  DificultadState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  void crearMenu();
  void cambiarTexturaMenu();
  int getDificultad();

  // Heredados de Ogre::Singleton.
  static DificultadState& getSingleton ();
  static DificultadState* getSingletonPtr ();

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;

  bool _exitGame;

 private:
  int _menuSeleccion;
  int _dificultad;
};

#endif
