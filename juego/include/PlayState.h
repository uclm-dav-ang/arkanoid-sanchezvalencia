#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "Bola.h"

#include "GameState.h"


class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
 public:
  PlayState () {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static PlayState& getSingleton ();
  static PlayState* getSingletonPtr ();

  int getNivel();
  Bola* getBola();
  std::list<Salvavidas*> getSalvavidas();
  void setNivel(int nivel);
  void setSalvavidas(std::list<Salvavidas*> lista_salvavidas);
  void setMovimientoBola(bool movimientoBola);

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;


  bool _exitGame;

 private:
  Ogre::SceneNode* _node_paleta;
  Ogre::SceneNode* _node_bola;
  Ogre::SceneNode* _node_limite_superior;
  Ogre::SceneNode* _node_limite_derecho;
  Ogre::SceneNode* _node_limite_izquierdo;
  Ogre::SceneNode* _node_space;
  Ogre::SceneNode* _node_game;
  bool _movimientoBola;
  bool _movPaletaRight;
  bool _movPaletaLeft;
  std::list<Bloque*> _vector_bloques;
  std::list<Salvavidas*> _lista_salvavidas;
  std::vector<char> _vector_nombre;
  char _nombre[20];

  int _nivel;
  Bola *_bola;
  Paleta *_paleta;
  Ogre::OverlayManager* _overlayManager;
  Ogre::AnimationState *_aimState;
};

#endif
