#ifndef INCLUDE_RECORDSSTATE_H_
#define INCLUDE_RECORDSSTATE_H_

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

class RecordsState : public Ogre::Singleton<RecordsState>, public GameState
{
 public:
  RecordsState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  void getMejoresPuntuaciones();

  // Heredados de Ogre::Singleton.
  static RecordsState& getSingleton ();
  static RecordsState* getSingletonPtr ();

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  Ogre::OverlayManager* _overlayManager;

  bool _exitGame;

 private:
  std::string _scoreFacil[3];
  std::string _scoreNormal[3];
  std::string _scoreDificil[3];

};

#endif
