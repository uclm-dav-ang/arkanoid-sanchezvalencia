#include <Ogre.h>
#include <cstdlib>
#include <vector>
using namespace Ogre;
using namespace std;

class Salvavidas {

private:
	SceneNode *_node;

public:
  Salvavidas(SceneNode* node);
  Salvavidas(const Salvavidas &obj);
  ~Salvavidas();
  Salvavidas& operator= (const Salvavidas &obj);
  SceneNode* getNode() const;
};
