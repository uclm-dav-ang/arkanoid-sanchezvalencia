#include <Ogre.h>
#include <cstdlib>
#include <vector>

using namespace Ogre;
using namespace std;

#define LIMITE_VERTICAL 6.65
#define TAM_LIMITE_VERTICAL 0.5
#define TAM_PALETAH 2.25
#define TAM_PALETAV 0.75
#define VEL_PALETA 0.1

class Paleta {

private:
	SceneNode *_node;
	double _velocidad_paleta;

public:
  Paleta(SceneNode* node);
  Paleta(const Paleta &obj);
  ~Paleta();
  Paleta& operator= (const Paleta &obj);
  SceneNode* getNode() const;
  void movRight();
  void movLeft();
};
