#include <Ogre.h>
#include <cstdlib>
#include <vector>

#include "Bloque.h"
#include "Paleta.h"
#include "Salvavidas.h"
#include "GameManager.h"

using namespace Ogre;
using namespace std;

#define LIMITE_SUPERIOR 8.4
#define TAM_BLOCK_H 1.0
#define TAM_BLOCK_V 0.5
#define TAM_BOLA 0.5
#define TAM_SALVAVIDAS_H 1.0
#define TAM_SALVAVIDAS_V 0.333
#define MODULO_F 0.15
#define MODULO_N 0.2
#define MODULO_D 0.25
#define LIM_VEL_ZF 0.14
#define LIM_VEL_ZN 0.19
#define LIM_VEL_ZD 0.24


class Bola{

private:
	SceneNode *_node;
	std::list<Bloque*> _vector_bloques;
	std::list<Salvavidas*> _lista_salvavidas;
	Paleta *_paleta;
	double _X;
	double _Z;
	int _vidas;
	int _puntos;
	GameManager *_gameManager;
	double _modulo;
	double _limite_velocidad_z;

public:
  Bola(SceneNode* node, std::list<Bloque*> vector_bloques, std::list<Salvavidas*> lista_salvavidas, Paleta *paleta);
  Bola(const Bola &obj);
  ~Bola();
  Bola& operator= (const Bola &obj);
  SceneNode* getNode() const;
  bool compruebaColision();
  void mover();
  void cambiarSentidoX();
  void cambiarSentidoZ();
  void calcularX();
  void calcularZ();
  void decrementarVidas();
  void incrementarPuntos();
  bool isWin();
  bool isLose();
  int getVidas();
  int getPuntos();
  void setBloques(std::list<Bloque*> vector_bloques);
  void setNode(SceneNode* node);
  void setPaleta(Paleta *paleta);
};
